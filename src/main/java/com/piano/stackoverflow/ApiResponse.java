package com.piano.stackoverflow;

import java.util.Date;

import org.springframework.web.util.HtmlUtils;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ApiResponse {
	private Question[] items;

	@JsonAlias({ "has_more" })
	private Boolean hasMore;

	/**
	 * @return the items
	 */
	public Question[] getItems() {
		return items;
	}

	/**
	 * @param items the items to set
	 */
	public void setItems(Question[] items) {
		this.items = items;
	}

	/**
	 * @return the hasMore
	 */
	public Boolean getHasMore() {
		return hasMore;
	}

	/**
	 * @param hasMore the hasMore to set
	 */
	public void setHasMore(Boolean hasMore) {
		this.hasMore = hasMore;
	}

	/**
	 * Transforms escaped string from Stack Exchange API back to normal string.
	 */
	public void unescapeStrings() {
		for (int i = 0; i < this.getItems().length; i++) {
			Question item = this.getItems()[i];
			if (item != null) {
				String escaped = HtmlUtils.htmlUnescape(item.getTitle());
				item.setTitle(escaped);

				Owner owner = item.getOwner();
				if (owner != null) {
					escaped = HtmlUtils.htmlUnescape(owner.getDisplayName());
					owner.setDisplayName(escaped);
				}
			}
		}
	}

	/**
	 * Transforms date from Stack Exchange API (milliseconds since standard unix
	 * time) back to standard date.
	 */
	public void transformDates() {
		for (int i = 0; i < this.getItems().length; i++) {
			Question item = this.getItems()[i];
			if (item != null) {
				Date transformed = new Date(item.getCreationDateUnixTime() * 1000);
				item.setCreationDate(transformed);
			}
		}
	}
}
