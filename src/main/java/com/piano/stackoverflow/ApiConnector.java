package com.piano.stackoverflow;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;

public class ApiConnector {
	private static final int pageSizeValue = 10;
	private static final String orderValue = "desc";
	private static final String sortValue = "relevance";
	private static final String siteValue = "stackoverflow";
	private static final String hostValue = "api.stackexchange.com";
	private static final String schemeValue = "http";
	private static final String pathValue = "/2.2/search";

	private static final String queryNullMessage = "query cannot be null";
	private static final String queryEmptyStringMessage = "query cannot be empty string";
	private static final String pageLessThan1Message = "page cannot be less than 1";

	private static final Logger logger = LoggerFactory.getLogger(ApiConnector.class);

	public static ApiResponse doSearch(String query, int page) throws Exception {
		if (query == null) {
			throw new IllegalArgumentException(queryNullMessage);
		}

		if (query.isEmpty()) {
			throw new IllegalArgumentException(queryEmptyStringMessage);
		}

		if (page < 1) {
			throw new IllegalArgumentException(pageLessThan1Message);
		}

		RestTemplate restTemplate = new RestTemplate();

		// fixing issues with gzipped content
		restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());

		String url = buildApiUrl(query, page);
		logger.debug(url);

		ApiResponse response;
		ObjectMapper objectMapper = new ObjectMapper();

		try {
			response = restTemplate.getForObject(url, ApiResponse.class);
			logger.debug(objectMapper.writeValueAsString(response));
		} catch (HttpClientErrorException exception) {
			ApiException apiException = objectMapper.readValue(exception.getResponseBodyAsString(), ApiException.class);

			logger.error(exception.getMessage(), exception);
			throw new Exception(
					String.format("Api Exception: %s - %s", apiException.getName(), apiException.getMessage()));
		}

		response.unescapeStrings();
		response.transformDates();

		return response;
	}

	public static String buildApiUrl(String query, int page) {
		if (query == null) {
			throw new IllegalArgumentException(queryNullMessage);
		}

		if (query.isEmpty()) {
			throw new IllegalArgumentException(queryEmptyStringMessage);
		}

		if (page < 1) {
			throw new IllegalArgumentException(pageLessThan1Message);
		}

		UriComponentsBuilder builder = UriComponentsBuilder.newInstance();

		builder.scheme(schemeValue).host(hostValue).path(pathValue);

		builder.queryParam("page", page);
		builder.queryParam("pageSize", pageSizeValue);
		builder.queryParam("order", orderValue);
		builder.queryParam("sort", sortValue);
		builder.queryParam("site", siteValue);
		builder.queryParam("intitle", query);

		return builder.build().toUriString();
	}
}
