package com.piano.stackoverflow;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Question {
	@JsonAlias({ "is_answered" })
	private Boolean isAnswered;

	@JsonAlias({ "creation_date" })
	private Long creationDateUnixTime;

	@JsonIgnore
	private Date creationDate;

	private String link;

	private String title;

	private Owner owner;

	/**
	 * @return the isAnswered
	 */
	public Boolean getIsAnswered() {
		return isAnswered;
	}

	/**
	 * @param isAnswered the isAnswered to set
	 */
	public void setIsAnswered(Boolean isAnswered) {
		this.isAnswered = isAnswered;
	}

	/**
	 * @return the creationDateUnixTime
	 */
	public Long getCreationDateUnixTime() {
		return creationDateUnixTime;
	}

	/**
	 * @param creationDateUnixTime the creationDateUnixTime to set
	 */
	public void setCreationDateUnixTime(Long creationDateUnixTime) {
		this.creationDateUnixTime = creationDateUnixTime;
	}

	/**
	 * @return the creationDate
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * @param creationDate the creationDate to set
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * @return the link
	 */
	public String getLink() {
		return link;
	}

	/**
	 * @param link the link to set
	 */
	public void setLink(String link) {
		this.link = link;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the owner
	 */
	public Owner getOwner() {
		return owner;
	}

	/**
	 * @param owner the owner to set
	 */
	public void setOwner(Owner owner) {
		this.owner = owner;
	}
}
