package com.piano.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.piano.stackoverflow.ApiConnector;
import com.piano.stackoverflow.ApiResponse;

@Controller
public class SearchController {

	@GetMapping("/")
	public String main(Model model) {
		model.addAttribute("main", true);
		return "search";
	}

	@GetMapping("/search")
	public String mainWithParam(@RequestParam(name = "page", required = true, defaultValue = "1") int page,
			@RequestParam(name = "query", required = true, defaultValue = "") String query, Model model) {
		Logger logger = LoggerFactory.getLogger(SearchController.class);

		try {
			ApiResponse resp = ApiConnector.doSearch(query, page);
			model.addAttribute("error", null);
			model.addAttribute("questions", resp.getItems());
			model.addAttribute("hasMore", resp.getHasMore());
		} catch (Exception exception) {
			model.addAttribute("error", exception.getMessage());
			logger.error(exception.getMessage(), exception);
		} finally {
			model.addAttribute("page", page);
			model.addAttribute("query", query);
			model.addAttribute("main", false);
		}

		return "search";
	}
}