package com.piano;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.piano.controller.SearchController;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = SearchController.class)
public class SearchControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void main() throws Exception {
		mockMvc.perform(get("/")).andExpect(status().isOk()).andExpect(view().name("search"))
				.andExpect(model().attribute("main", equalTo(true)))
				.andExpect(content().string(containsString("Search")));
	}

	@Test
	public void search() throws Exception {
		mockMvc.perform(get("/search").param("query", "class").param("page", "1")).andExpect(status().isOk())
				.andExpect(view().name("search")).andExpect(model().attribute("main", false))
				.andExpect(model().attribute("page", 1)).andExpect(model().attribute("query", "class"));
	}
}
